#-------------------------------------------------------------------------------
# Name:        GetMovies
# Purpose:
#
# Author:      Coolee2020
#
# Created:     22/03/2022
# Copyright:   (c) Administrator 2022
# Licence:     <GPL 2.0>
#-------------------------------------------------------------------------------
import random
import requests
import re
import csv
from lxml import etree

def getheaders():
    user_agent_list = ['Mozilla/5.0 (Windows NT 6.2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1464.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.16 Safari/537.36',
    'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.3319.102 Safari/537.36',
    'Mozilla/5.0 (X11; CrOS i686 3912.101.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.116 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.93 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36',
    'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:17.0) Gecko/20100101 Firefox/17.0.6',
    'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1468.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2224.3 Safari/537.36',
    'Mozilla/5.0 (X11; CrOS i686 3912.101.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.116 Safari/537.36']
    return random.choice(user_agent_list)

xpaths=[]
urls=[]

def makeurl():
    for i in range(1,11):
        urls.append('https://ssr1.scrape.center/page/{0}'.format(i))

def makepath():
    for i in range(1,11):
        xpaths.append('//*[@id="index"]/div[1]/div[1]/div[{0}]/div/div/div[2]/a/h2'.format(i))
        xpaths.append('//*[@id="index"]/div[1]/div[1]/div[{0}]/div/div/div[2]/div[1]/button/span'.format(i))
        xpaths.append('//*[@id="index"]/div[1]/div[1]/div[{0}]/div/div/div[2]/div[2]/span[1]'.format(i))
        xpaths.append('//*[@id="index"]/div[1]/div[1]/div[{0}]/div/div/div[2]/div[2]/span[3]'.format(i))
        xpaths.append('//*[@id="index"]/div[1]/div[1]/div[{0}]/div/div/div[2]/div[3]/span'.format(i))
        xpaths.append('//*[@id="index"]/div[1]/div[1]/div[{0}]/div/div/div[3]/p[1]'.format(i))


def getxpath(i):
    if i <= len(xpaths)-1 and i >= 0:
        return xpaths[i]
    else:
        return None

def get_one_page(url):
    headers={}
    headers['User-Agent'] = getheaders()
    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        return response.text
    return None

def main():
    headers =['片名','类型','地区','时长','上映时间','评分'];
    makepath()
    makeurl()
    with open('movie.csv','w',encoding='utf-8',newline='') as f:
        print('Start spider....')
        f_csv = csv.writer(f)
        f_csv.writerow(headers)
        for url in urls:
            ret = get_one_page(url)
            if ret !=None:
                html = etree.HTML(ret)
                index = 0
                xp =getxpath(index)
                cellindex = 0
                row=[]
                while xp != None:

                    html_data = html.xpath(xp)
                    cellstr =''
                    for i in html_data:
                        if type(i) == etree._Element:
                            cellstr = cellstr +' '+i.text
                        else:
                            cellstr = cellstr +' '+i
                    row.append(cellstr.strip())
                    index = index + 1
                    xp = getxpath(index)
                    cellindex = cellindex + 1
                    if cellindex == len(headers):
                        cellindex = 0
                        f_csv.writerow(row)
                        row.clear()

        print('All OK!')

if __name__ == '__main__':
    main()
